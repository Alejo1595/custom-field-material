import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MyTel } from './components/my-tel-input/my-tel-input.component';
import { MyCustomInput } from './components/custom-input/custom-input.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'custom-from-material';

  formulario: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formulario = fb.group({
      telefono: new MyTel('', '', ''),
      presion: new MyCustomInput('', '',)
    })
  }

  public onSubmit() {
    console.log(this.formulario.value);
  }
}
